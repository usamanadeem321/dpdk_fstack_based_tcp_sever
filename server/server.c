#include <stdio.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <errno.h>
#include <assert.h>
#include <unistd.h>

#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <netinet/tcp.h>

#include "ff_config.h"
#include "ff_api.h"
#include "ff_epoll.h"

#define MAX_EVENTS 512

#define likely(x) __builtin_expect((x), 1)
#define unlikely(x) __builtin_expect((x), 0)

struct epoll_event ev;
struct epoll_event events[MAX_EVENTS];

int port = 8081;
char ip[] = "192.168.122.1";

int epfd;   //epoll fd
int sockfd; //socket fd

int connected = 0;
int sequence_num = 0;
char send_buf[1024];
char recv_buf[1024];
int waiting_receive = 0;

int nevents;
int event_itr;

size_t readlen;

struct timespec start_time, end_time;

int packet_seq_no;
double latency;

int sequence_number_len;

int loop(void *arg)
{
    /* Wait for events to happen */

    int nevents = ff_epoll_wait(epfd, events, MAX_EVENTS, 0);
    int i;

    for (i = 0; i < nevents; ++i)
    {
        /* Handle new connect */
        if (unlikely(events[i].data.fd == sockfd))
        {
            while (1)
            {
                printf("In sockfd event\n");
                int nclientfd = ff_accept(sockfd, NULL, NULL);
                if (nclientfd < 0)
                {
                    break;
                }

                /* Add to event list */
                ev.data.fd = nclientfd;
                ev.events = EPOLLIN;
                if (ff_epoll_ctl(epfd, EPOLL_CTL_ADD, nclientfd, &ev) != 0)
                {
                    printf("ff_epoll_ctl failed:%d, %s\n", errno,
                           strerror(errno));
                    break;
                }
            }
        }
        else
        {
            if (likely(events[i].events & EPOLLIN))
            {

                size_t readlen = ff_read(events[i].data.fd, recv_buf, 1024);
                printf("Client: %s", recv_buf);

                if (readlen > 0)
                {
                    printf("Write back to client... \n");
                    ff_write(events[i].data.fd, recv_buf, readlen);
                }
                else
                {
                    ff_epoll_ctl(epfd, EPOLL_CTL_DEL, events[i].data.fd, NULL);
                    ff_close(events[i].data.fd);
                }
            }
            else if (unlikely(events[i].events & EPOLLERR))
            {
                /* Simply close socket */
                ff_epoll_ctl(epfd, EPOLL_CTL_DEL, events[i].data.fd, NULL);
                ff_close(events[i].data.fd);
            }
            else
            {
                printf("unknown event: %8.8X\n", events[i].events);
            }
        }
    }
    return 0;
}



int init_socket(int socket, int port)
{
    socket = ff_socket(AF_INET, SOCK_STREAM, 0);
    printf("socket:%d\n", socket);
    if (socket < 0)
    {
        printf("ff_socket failed\n");
        return 1;
    }

    // allow multiple sockets to use the same PORT number
    u_int yes = 1;
    if (
        ff_setsockopt(
            socket, SOL_SOCKET, SO_REUSEADDR, (char *)&yes, sizeof(yes)) < 0)
    {
        perror("Reusing ADDR failed");
        return 1;
    }

    int on = 1;
    ff_ioctl(socket, FIONBIO, &on);

    struct sockaddr_in addr;
    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    int tcp_nodelay_flag = 1;
    ff_setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY, (void *)&tcp_nodelay_flag, sizeof(tcp_nodelay_flag));

   
        int ret = ff_bind(socket, (struct linux_sockaddr *)&addr, sizeof(addr));
        if (ret < 0)
        {
            printf("ff_bind failed\n");
            return 1;
        }
        else
        {
            printf("bind successful\n");
        }

        ret = ff_listen(socket, MAX_EVENTS);
        if (ret < 0)
        {
            printf("ff_listen failed\n");
            exit(1);
        }
 
    return socket;
}

int main(int argc, char *argv[])
{

    ff_init(argc, argv);



    sockfd = init_socket(sockfd, port);

    assert((epfd = ff_epoll_create(0)) > 0);
    ev.data.fd = sockfd;
    ev.events = EPOLLIN;
    ff_epoll_ctl(epfd, EPOLL_CTL_ADD, sockfd, &ev);

 
        ff_run(loop, NULL);
  
    return 0;
}
