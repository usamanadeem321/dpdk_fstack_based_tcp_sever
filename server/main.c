#include <stdio.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <errno.h>
#include <assert.h>

#include "ff_config.h"
#include "ff_api.h"
#include "ff_epoll.h"

#define MAX_EVENTS 512

int PORT = 14310;
int nevents;
int waiting_receive = 0;
int connected = 0;
int sequence_num = 0;
struct epoll_event ev;
struct epoll_event events[MAX_EVENTS];
char ip[] = "192.168.122.1";
int epfd;
int sockfd;
int event_itr;
size_t readlen;
char send_buf[1024];
char recv_buf[1024];

int sequence_number_len;

void write()
{
    printf("**********WRITE********");
}

int loop(void *arg)
{
    //int nevents = ff_epoll_wait(epfd, events, MAX_EVENTS, 0);

    printf("Listening ... \n");
    int ret = ff_listen(sockfd, MAX_EVENTS);
    if (ret < 0)
    {
        printf("ff_listen failed\n");
        exit(1);
    }

    if (ff_accept(sockfd, NULL, NULL) == 0)

        write();
}

int pinger_loop(void *arg)
{
    /* Wait for events to happen */

    if (connected == 0)
    {
        int on = 1;
        ff_ioctl(sockfd, FIONBIO, &on);

        struct sockaddr_in addr;
        bzero(&addr, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_port = htons(PORT);
        inet_pton(AF_INET, ip, &addr.sin_addr);

        int ret = ff_connect(sockfd, (struct linux_sockaddr *)&addr, sizeof(addr));
        if (ret < 0)
        {
            printf("ff_connect failed: %d %d\n", ret, errno);
            if (errno == 106)
            {
                connected = 1;
                printf("connection success\n");
            }
            else
            {
                return 0;
            }
        }
    }

    return 0;
}

int main(int argc, char *argv[])
{
    ff_init(argc, argv);

    sockfd = ff_socket(AF_INET, SOCK_STREAM, 0);

    int on = 1;
    ff_ioctl(sockfd, FIONBIO, &on);

    struct sockaddr_in addr;
    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT);
    inet_pton(AF_INET, ip, &addr.sin_addr);

    // assert((epfd = ff_epoll_create(0)) > 0);
    // ev.data.fd = sockfd;
    // ev.events = EPOLLIN;
    // ff_epoll_ctl(epfd, EPOLL_CTL_ADD, sockfd, &ev); //add fd to epoll object

    printf("Binding ... \n");
    int ret = ff_bind(sockfd, (struct linux_sockaddr *)&addr, sizeof(addr));
    if (ret < 0)
    {
        printf("ff_bind failed\n");
        exit(1);
    }
    else
        printf("Binded Succesfully!\n");

    printf("Starting main loop ... \n");
    ff_run(pinger_loop, NULL);
    return 0;
}
