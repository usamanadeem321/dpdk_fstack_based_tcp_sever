#include <stdio.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <errno.h>
#include <assert.h>
#include <unistd.h>

#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <netinet/tcp.h>

#include "ff_config.h"
#include "ff_api.h"
#include "ff_epoll.h"

#define MAX_EVENTS 512
char buff[80];
#define likely(x) __builtin_expect((x), 1)
#define unlikely(x) __builtin_expect((x), 0)

struct epoll_event ev;
struct epoll_event events[MAX_EVENTS];

int port = 8081;
char ip[] = "192.168.122.1";

int epfd;   //epoll fd
int sockfd; //socket fd
int pinger = 0;
int ponger = 0;

int connected = 0;
int sequence_num = 0;
char send_buf[1024];
char recv_buf[1024];
int waiting_receive = 0;

int nevents;
int event_itr;

size_t readlen;


int packet_seq_no;
double latency;

int sequence_number_len;



int pinger_loop(void *arg)
{
    /* Wait for events to happen */

    if (unlikely(connected == 0))
    {
        int on = 1;
        ff_ioctl(sockfd, FIONBIO, &on);

        struct sockaddr_in addr;
        bzero(&addr, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_port = htons(port);
        inet_pton(AF_INET, ip, &addr.sin_addr);

        int ret = ff_connect(sockfd, (struct linux_sockaddr *)&addr, sizeof(addr));
        if (ret < 0)
        {
            printf("ff_connect failed: %d %d\n", ret, errno);
            if (errno == 106)
            {
                connected = 1;
                printf("connection success\n");
            }
            else
            {
                return 0;
            }
        }
    }

    if (likely(waiting_receive))
    {
        nevents = ff_epoll_wait(epfd, events, MAX_EVENTS, 0);
        for (event_itr = 0; event_itr < nevents; ++event_itr)
        {
            readlen = ff_read(events[event_itr].data.fd, (void *)recv_buf, 1024);
            printf("Server: %s", recv_buf);



            //printf("received = %s\n",recv_buf);
            waiting_receive = 0;
            return 0;
        }
    }
    else
    {
        bzero(buff, sizeof(buff));
        printf("Enter the string : ");
        int n = 0;
        while ((buff[n++] = getchar()) != '\n')
            ;

        ff_write(sockfd, buff, sizeof(buff));
        waiting_receive = 1;

        return 0;
    }
    return 0;
}

int init_socket(int socket, int port)
{
    socket = ff_socket(AF_INET, SOCK_STREAM, 0);
    printf("socket:%d\n", socket);
    if (socket < 0)
    {
        printf("ff_socket failed\n");
        return 1;
    }

    // allow multiple sockets to use the same PORT number
    u_int yes = 1;
    if (
        ff_setsockopt(
            socket, SOL_SOCKET, SO_REUSEADDR, (char *)&yes, sizeof(yes)) < 0)
    {
        perror("Reusing ADDR failed");
        return 1;
    }

    int on = 1;
    ff_ioctl(socket, FIONBIO, &on);

    struct sockaddr_in addr;
    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    int tcp_nodelay_flag = 1;
    ff_setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY, (void *)&tcp_nodelay_flag, sizeof(tcp_nodelay_flag));


    return socket;
}

int main(int argc, char *argv[])
{

    ff_init(argc, argv);

  pinger=1;

    sockfd = init_socket(sockfd, port);

    printf("Epoll Creation ...\n");
    assert((epfd = ff_epoll_create(0)) > 0);
    ev.data.fd = sockfd;
    ev.events = EPOLLIN;
    ff_epoll_ctl(epfd, EPOLL_CTL_ADD, sockfd, &ev);

        ff_run(pinger_loop, NULL);


    return 0;
}
